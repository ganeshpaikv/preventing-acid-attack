/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class AcidContract extends Contract {

    async acidExists(ctx, acidId) {
        const buffer = await ctx.stub.getState(acidId);
        return (!!buffer && buffer.length > 0);
    }

    async createAcid(ctx, acidId, name, quantity) {
        const mspID = ctx.clientIdentity.getMSPID();
        if(mspID === 'manufacturer-acid-com'){
            const exists = await this.acidExists(ctx, acidId);
            if (exists) {
                throw new Error(`The acid ${acidId} already exists`);
            }
            const asset = { name, quantity, status:'In Factory' , assetType: 'acid'};
            const buffer = Buffer.from(JSON.stringify(asset));
            await ctx.stub.putState(acidId, buffer);

            let addAcidEventData = { Type: 'Acid creation'};
            await ctx.stub.setEvent('addAcidEvent', Buffer.from(JSON.stringify(addAcidEventData)));
        }
        else{
            return 'The user is not authorised to perform this action';
        }

    }

    async readAcid(ctx, acidId) {
        const exists = await this.acidExists(ctx, acidId);
        if (!exists) {
            throw new Error(`The acid ${acidId} does not exist`);
        }
        const buffer = await ctx.stub.getState(acidId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    // async updateAcid(ctx, acidId, newValue) {
    //     const exists = await this.acidExists(ctx, acidId);
    //     if (!exists) {
    //         throw new Error(`The acid ${acidId} does not exist`);
    //     }
    //     const asset = { value: newValue };
    //     const buffer = Buffer.from(JSON.stringify(asset));
    //     await ctx.stub.putState(acidId, buffer);
    // }

    async deleteAcid(ctx, acidId) {

        const mspID = ctx.clientIdentity.getMSPID();
        if(mspID === 'manufacturer-acid-com'){
            const exists = await this.acidExists(ctx, acidId);
            if (!exists) {
                throw new Error(`The acid ${acidId} does not exist`);
            }
            await ctx.stub.deleteState(acidId);
        }
        else{
            return 'The user is not authorised to perform this action';
        }
    }

}

module.exports = AcidContract;
