/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const crypto = require('crypto');

// async function getCollectionName(ctx) {
//     const mspid = ctx.clientIdentity.getMSPID();
//     const collectionName = `_implicit_org_${mspid}`;
//     return collectionName;
// }

const collectionName = 'CollectionValidation';

class ValidationContract extends Contract {

    async validationExists(ctx, validationId) {
        // const collectionName = await getCollectionName(ctx);
        const data = await ctx.stub.getPrivateDataHash(collectionName, validationId);
        return (!!data && data.length > 0);
    }

    async createValidation(ctx, validationId) {
        const mspID = ctx.clientIdentity.getMSPID();
        if(mspID === 'lab-acid-com'){
            const exists = await this.validationExists(ctx, validationId);
            if (exists) {
                throw new Error(`The asset validation ${validationId} already exists`);
            }

            // const privateAsset = {};

            const transientData = ctx.stub.getTransient();
            if (transientData.size === 0 ||
            !transientData.has('name')||
            !transientData.has('mobilenumber')||
            !transientData.has('aadhaarnumber')){
                throw new Error('The data shared is insufficient. Please try again.');
            }

            const validationAsset = {};

            validationAsset.name = transientData.get('name').toString();
            validationAsset.mobilenumber = transientData.get('mobilenumber').toString();
            validationAsset.aadhaarnumber = transientData.get('aadhaarnumber').toString();
            validationAsset.type = 'validation';
            // privateAsset.privateValue = transientData.get('privateValue').toString();

            // const collectionName = await getCollectionName(ctx);
            await ctx.stub.putPrivateData(collectionName, validationId, Buffer.from(JSON.stringify(validationAsset)));
            return true;
        }
    }
    async readValidation(ctx, validationId) {
        const exists = await this.validationExists(ctx, validationId);
        if (!exists) {
            throw new Error(`The asset validation ${validationId} does not exist`);
        }
        let privateDataString;
        // const collectionName = await getCollectionName(ctx);
        const privateData = await ctx.stub.getPrivateData(collectionName, validationId);
        privateDataString = JSON.parse(privateData.toString());
        return privateDataString;
    }

    async updateValidation(ctx, validationId) {
        const exists = await this.validationExists(ctx, validationId);
        if (!exists) {
            throw new Error(`The asset validation ${validationId} does not exist`);
        }
        const privateAsset = {};

        const transientData = ctx.stub.getTransient();
        if (transientData.size === 0 || !transientData.has('privateValue')) {
            throw new Error('The privateValue key was not specified in transient data. Please try again.');
        }
        privateAsset.privateValue = transientData.get('privateValue').toString();

        // const collectionName = await getCollectionName(ctx);
        await ctx.stub.putPrivateData(collectionName, validationId, Buffer.from(JSON.stringify(privateAsset)));
    }

    async deleteValidation(ctx, validationId) {
        const exists = await this.validationExists(ctx, validationId);
        if (!exists) {
            throw new Error(`The asset validation ${validationId} does not exist`);
        }
        // const collectionName = await getCollectionName(ctx);
        await ctx.stub.deletePrivateData(collectionName, validationId);
    }

    async verifyValidation(ctx, mspid, validationId, objectToVerify) {

        // Convert provided object into a hash
        const hashToVerify = crypto.createHash('sha256').update(objectToVerify).digest('hex');
        const pdHashBytes = await ctx.stub.getPrivateDataHash(`_implicit_org_${mspid}`, validationId);
        if (pdHashBytes.length === 0) {
            throw new Error('No private data hash with the key: ' + validationId);
        }

        const actualHash = Buffer.from(pdHashBytes).toString('hex');

        // Compare the hash calculated (from object provided) and the hash stored on public ledger
        if (hashToVerify === actualHash) {
            return true;
        } else {
            return false;
        }
    }


}

module.exports = ValidationContract;
