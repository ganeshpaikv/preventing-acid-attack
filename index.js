/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const AcidContract = require('./lib/acid-contract');
const OrderContract = require('./lib/order-contract');
const ValidationContract = require('./lib/validation-contract');
module.exports.AcidContract = AcidContract;
module.exports.OrderContract = OrderContract;
module.exports.ValidationContract = ValidationContract;
module.exports.contracts = [ AcidContract , OrderContract ,ValidationContract ];
